from server import app
from flask import Flask, render_template, request, redirect, session, g, abort, url_for, flash, send_file
import os
import database
import _pickle as pickle
import notification as notif
from flask.json import JSONEncoder
from datetime import datetime, timedelta
from functools import wraps
from passlib.hash import sha256_crypt

######################################
# Defining Global Variables
######################################
app.secret_key = os.urandom(24)
app.json_encoder = JSONEncoder
user_ans = []
count = 0
@app.before_request
def make_session_permanent():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=100)

def login_required(f):
	@wraps(f)
	def wrap(*args, **kwargs):
		if 'logged_in' in session:
			return f(*args, **kwargs)
		if 'message' in session:
			message = session['message']
			session.pop('message')
		else:
			message = "Please enter your username and password:"
		return render_template('login.html', message = message)
	return wrap

@app.route('/')
@login_required
def hello_world():
    return render_template("index.html", user = session['username'])

@app.route('/mcq/')
@login_required
def mcq():
	global count
	del user_ans[:]
	count = 0
	i = 1
	all_ans = database.get_mcq()
	error = ''
	if 'message' in session:
		error = session['message']
		session['message'] = ''
	return render_template('mcq.html',i = i, len = len(all_ans), all_ans = all_ans)

@app.route('/next_ques/', methods = ['GET','POST'])
@login_required
def next_ques():
	global count
	status = ""
	all_ans = database.get_mcq()
	i = request.form['curr_value']
	ans = request.form['ans']
	user_ans.append(ans)
	if all_ans[int(i)]["answer"] in user_ans[int(i)-1]:
		count = count + 1
	
	print("count = "+str(count))
	error = ''
	if 'message' in session:
		error = session['message']
		session['message'] = ''

	if( all_ans[int(i)]["answer"] == ans):
		status = "Correct"
	else:
		status = "Incorrect"

	database.push_mcq_frequency(int(i), all_ans[int(i)]["subId"], all_ans[int(i)]["answer"], ans, status)
	if (int(i) < len(all_ans)):
		return render_template('mcq.html',i = int(i)+1 , len = len(all_ans), all_ans = all_ans )
	else:
		return render_template('show_result.html', user_ans = user_ans, all_ans = all_ans, len = len(all_ans), count = count)

@app.route("/logout/")
@login_required
def logout():
	session['logged_in'] = False

	#remove all elements of user's session
	keys = list(session.keys())
	for key in keys:
		session.pop(key)

	return redirect('/')


@app.route('/login/', methods=['POST', 'GET'])
def do_admin_login():
	if request.method == 'GET':
		return redirect('/')
	
	if 'logged_in' in session:
		return redirect('/')

	login_dict = database.get_logins()


	password = request.form['password']
	username = request.form['username']

	if not username in login_dict:
		session['message'] = "Invalid Username"
		return redirect('/')

	encrypted_password = login_dict[username]['password']
	# is_valid = encrypted_password == password
	is_valid = sha256_crypt.verify(password, encrypted_password)

	if not is_valid:
		session['message'] = "Incorrect Password"
	else:
		session['logged_in'] = True
		session['username'] = username

	return redirect('/')

@app.route('/new_account/')
# @login_required
def new_account():
	error = ''
	if 'message' in session:
		error = session['message']
		session['message'] = ''
	return render_template('new_account.html', error = error)

@app.route('/new_account_submit/', methods = ['GET','POST'])
# @login_required
def new_account_submit():

	if request.method == 'GET':
		return redirect('/new_account/')

	email = "ankan"

	email = request.form['email']
	username = request.form['username']
	password = request.form['password']

	#validating email
	login_email = database.get_emails()
	if email in login_email:
		session['message'] = 'Email already exists'
		return redirect('/new_account/')

	#validate submitted username
	login_dict = database.get_logins()
	if username in login_dict:
		session['message'] = 'Username already exists'
		return redirect('/new_account/')

	#encrypt and store new passord
	# encrypted_password = sha256_crypt.encrypt(password)
	# database.create_login(username, encrypted_password)
	# session['message'] = "New account created for {}".format(username)
	try:
		totpobj, totp = notif.OTP().send_otp(email)
		session['totp_object'] = pickle.dumps(totpobj)
	except:
		session['message'] = 'Error in sending or generating otp'
		render_template('new_account.html')
	return render_template('verify.html', username=username, email=email, password=password)

@app.route('/verify_id/', methods=["GET", "POST"])
def verify_id():
	if request.method == 'GET':
		return redirect('/new_account/')
	totpobj = pickle.loads(session['totp_object'])
	session['totp_object'] = ''
	password = request.form['password']
	username = request.form['username']
	email = request.form['email']
	totpval = request.form['totp']
	if totpobj.verify(totpval):
		encrypted_password = sha256_crypt.hash(password)
		database.create_login(email, username, encrypted_password)
		session['message'] = "New account created for {}".format(username)
		return redirect('/confirm/')
	else:
		session['message'] = "OTP verification failed for Email"
		return redirect('/new_account/')
		


@app.route('/confirm/')
@login_required
def confirm():
	message = ''
	if 'message' in session:
		message = session['message']
		session['message'] = ''
	return render_template('confirm.html', message = message)


# @app.errorhandler(404)
# @app.route("/error404")
# def page_not_found(error):
#     return app.send_static_file('404.html')

# @app.errorhandler(500)
# @app.route("/error500")
# def requests_error(error):
#     return app.send_static_file('500.html')
