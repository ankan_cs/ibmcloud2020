import logging
import json
import ibm_db

# logging.basicConfig(level=logging.DEBUG, filename='app.log', filemode='w', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.basicConfig(level=logging.DEBUG)
logging.debug('This is a debug message')
logging.info('This is an info message')
logging.warning('This is a warning message')
logging.error('This is an error message')

######################################
# Defining Global Variables
######################################
f = open("config/database.json")
db2creds = json.load(f)
conf = db2creds['DB2']
dbschema = conf['schema']

def connect():
    # logging.debug('db2creds: {}\n'.format(db2creds))
    try:
        print("Connecting: {}, {}, {}, {}, {}".format(conf['db'],conf['username'],conf['password'],conf['host'],conf['port']))
        # conn = ibm_db.connect(dsn= db2creds['DB2']['dsn'], uid=db2creds['DB2']['username'], pwd=db2creds['DB2']['password'])
        conn = ibm_db.connect(conf['dsn'], "", "")
        # conn = psycopg2.connect(database=conf['db'],
		# 						user=conf['username'],
		# 						password=conf['password'],
		# 						host=conf['host'],
		# 						port=conf['port'])
        print("Connected: {}, {}, {}, {}, {}".format(conf['db'],conf['username'],conf['password'],conf['host'],conf['port']))
    except:
        raise

    # cur = conn.cursor()
    print("Successfully connected")

    return conn

def get_logins():
    conn = connect()
    login_dict = {}
    dbschema = conf['schema']
    sql = 'select user_name as u, password as p from {}.user_creds'.format(dbschema)
    print('SQL: '+sql)

    stmt = ibm_db.exec_immediate(conn, sql)
    login_tuple = ibm_db.fetch_tuple(stmt)

    print('login_tuple: \n{}'.format(login_tuple))

    while login_tuple != False:
        login_dict[login_tuple[0]] = {
            'password': login_tuple[1]
        }
        login_tuple = ibm_db.fetch_tuple(stmt)
    return login_dict

def get_emails():
    # login_email = ['ankan@gmail.com','bhattacharyya@gmail.com']
    conn = connect()
    login_email = []
    
    sql = 'select email_id as email from {}.user_creds'.format(dbschema)
    print('SQL: '+sql)

    stmt = ibm_db.exec_immediate(conn, sql)
    login_tuple = ibm_db.fetch_tuple(stmt)

    print('login_tuple: \n{}'.format(login_tuple))

    while login_tuple!=False:
        login_email.append(login_tuple[0])
        login_tuple = ibm_db.fetch_tuple(stmt)


    return login_email

def get_mcq():
    conn = connect()
    mcq_details = {}
    dbschema = conf['schema']
    sql = 'select * from {}.mcq_new'.format(dbschema)
    print('SQL: '+sql)
 
    stmt = ibm_db.exec_immediate(conn, sql)
    mcq_tuple = ibm_db.fetch_tuple(stmt)
 
    print('mcq_tuple: \n{}'.format(mcq_tuple))
 
    while mcq_tuple != False:
        mcq_details[mcq_tuple[1]] = {
            'subId': mcq_tuple[1],
            'question': mcq_tuple[2],
            'option1': mcq_tuple[3],
            'option2': mcq_tuple[4],
            'option3': mcq_tuple[5],
            'answer': mcq_tuple[6]
        }
        mcq_tuple = ibm_db.fetch_tuple(stmt)
    return mcq_details


def get_questions():
    conn = connect()
    id = []
    subid = []
    ques = []
    option1 = []
    option2 = []
    option3 = []
    ans = []
    sql = 'select * from {}.mcq'.format(dbschema)
    print('SQL: '+sql)

    stmt = ibm_db.exec_immediate(conn, sql)
    questions_tuple = ibm_db.fetch_tuple(stmt)

    print('questions_tuple: \n{}'.format(questions_tuple))

    while questions_tuple!=False:
        id.append(questions_tuple[0])
        subid.append(questions_tuple[1])
        ques.append(questions_tuple[2])
        option1.append(questions_tuple[3])
        option2.append(questions_tuple[4])
        option3.append(questions_tuple[5])
        ans.append(questions_tuple[6])
        questions_tuple = ibm_db.fetch_tuple(stmt)


    return id, subid, ques, option1, option2, option3, ans


def create_login(email, username, password):
    # logging.debug("create login: username: {}, password: {}".format(username, password))
    # array = { ibm_db.SQL_ATTR_AUTOCOMMIT : ibm_db.SQL_AUTOCOMMIT_OFF }
    conn = connect()
    sql = 'insert into {}.user_creds (email_id, user_name, password) values (\'{}\', \'{}\', \'{}\')'.format(dbschema, email, username, password)
    print('sql: {}'.format(sql))
    try:
        stmt = ibm_db.exec_immediate(conn, sql)        
    except:
        raise

    return 'create_login'

def push_mcq_frequency(i, qno, cans, uans, status):
    conn = connect()
    sql = 'insert into {}.mcq_frequency (id, quesno, cans, uans, status) values (\'{}\', \'{}\', \'{}\', \'{}\', \'{}\')'.format(dbschema, i, qno, cans, uans, status)
    print('sql: {}'.format(sql))
    try:
        stmt = ibm_db.exec_immediate(conn, sql)        
    except:
        raise
    
    return 'push_mcq_frequency'

def change_password(username, password):
	return 'change_password'

def disconnect(conn, cur):
	return 'disconnected'
